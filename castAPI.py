from flask import Flask, request
import paramiko
import flask_cors
import os

app = Flask(__name__)
flask_cors.CORS(app)

host = 'fe80::7e76:35ff:feb0:cd1d'
port = 22
username = 'kiosk'
password = 'DBSRocks!85'


@app.route("/")
def index():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, port=port, username=username, password=password)

    ftp_client = ssh.open_sftp()
    ftp_client.put('welcome.html', 'welcome.html')
    ftp_client.close()

    stdin, stdout, stderr = ssh.exec_command('pkill -f chromium-browser')
    stdin, stdout, stderr = ssh.exec_command('DISPLAY=:0 chromium-browser welcome.html --kiosk --incognito')

    ssh.close()

    return 'loaded!'


@app.route('/video', methods=["POST"])
def video_sender():
    video_data = request.get_json()
    video_id = video_data['id']
    style = video_data['style']
    text_overlay = video_data['text_overlay']
    scripts = video_data['scripts']
    directory = video_data['directory']

    create_html_file(video_id, style, text_overlay, scripts, directory)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, port=port, username=username, password=password)

    ftp_client = ssh.open_sftp()
    ftp_client.put('file.html', 'file.html')
    ftp_client.close()

    stdin, stdout, stderr = ssh.exec_command('pkill -f chromium-browser')
    stdin, stdout, stderr = ssh.exec_command('DISPLAY=:0 chromium-browser --kiosk --incognito')
    stdin, stdout, stderr = ssh.exec_command('pkill -f chromium-browser')
    stdin, stdout, stderr = ssh.exec_command('DISPLAY=:0 chromium-browser file.html --kiosk --incognito')

    print('Sent video to tv!')

    if stdout:
        for line in stdout:
            print(line)
    else:
        print('no stdout')


@app.route('/url', methods=['POST'])
def url_sender():
    url_data = request.get_json()
    url = url_data['url']

    prefix = url.split('://')[0]

    if prefix != 'http' and prefix != 'https':
        url = 'https://www.' + url

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, port=port, username=username, password=password)

    stdin, stdout, stderr = ssh.exec_command('pkill -f chromium-browser')
    stdin, stdout, stderr = ssh.exec_command('DISPLAY=:0 chromium-browser --kiosk --disable-session-crashed-bubble --disable-infobars --incognito')
    stdin, stdout, stderr = ssh.exec_command('pkill -f chromium-browser')
    stdin, stdout, stderr = ssh.exec_command('DISPLAY=:0 chromium-browser --app=' + url + ' --disable-session-crashed-bubble --disable-infobars --kiosk')

    print('Sent url to tv!')

    if stdout:
        for line in stdout:
            print(line)
    else:
        print('no stdout')


def create_html_file(video_id, style, text_overlay, scripts, directory):
    filename = directory + "/file.html"

    head = """<!doctypehtml><html lang=en><meta charset=UTF-8><meta content="width=device-width,initial-scale=1"name=viewport><meta content="ie=edge"http-equiv=X-UA-Compatible><title>Video Ready</title><style>#myVideo{position:fixed;right:0;bottom:0;min-width:100%;min-height:100%}.vimeo-wrapper{position:fixed;top:0;left:0;width:100%;height:100%;z-index:-1;pointer-events:none;overflow:hidden}.vimeo-wrapper iframe{width:100vw;height:56.25vw;min-height:100vh;min-width:177.77vh;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}"""
    video_1 = """</style></head><body><div class="vimeo-wrapper"><iframe src="https://player.vimeo.com/video/"""
    video_2 = """?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen allow=autoplay></iframe></div>"""
    foot = """</body></html>"""

    html = head + \
           style + \
           video_1 + \
           video_id + \
           video_2 + \
           text_overlay + \
           "<script>" + \
           scripts + \
           "</script>" + \
           foot

    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))

    with open(filename, 'w+') as f:
        f.write(html)


@app.route('/kill')
def kill():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, port=port, username=username, password=password)

    stdin, stdout, stderr = ssh.exec_command('pkill -f chromium-browser')
    stdin, stdout, stderr = ssh.exec_command('rm test_min.html')
    stdin, stdout, stderr = ssh.exec_command('rm file.html')

    ssh.close()

    print('killt!')


if __name__ == '__main__':
    app.run(debug=True, port=4000)
